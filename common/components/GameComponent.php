<?php
/**
 * @author Denysaw
 */
namespace common\components;

use Yii;
use yii\base\Component;
use common\models\Toss;
use common\models\Gift;
use common\models\Prize;
use common\models\Budget;

/**
 * Class GameComponent
 * @package common\components
 */
class GameComponent extends Component
{

    /**
     * @return Toss
     */
    public function tossPrizes(): Toss
    {
        /** @var Prize $prize */
        $prize = Prize::find()->orderBy('rand()')->one();
        $data  = null;

        $toss = new Toss();
        $toss->user_id  = Yii::$app->getUser()->id;
        $toss->prize_id = $prize->id;

        switch ($prize->type) {
            case Prize::TYPE_MONEY:
                $money  = Yii::$app->params['money'];
                $min    = $money['min'] / $money['step'];
                $max    = $money['max'] / $money['step'];
                $budget = Budget::main()->amount / $money['step'];
                $data   = $budget ? rand($min, $max <= $budget ? $max : $budget) * $money['step'] : 0;
                break;

            case Prize::TYPE_TOPUP:
                $points = Yii::$app->params['points'];
                $min    = $points['min'] / $points['step'];
                $max    = $points['max'] / $points['step'];
                $data   = rand($min, $max) * $points['step'];
                break;

            case Prize::TYPE_GIFT:
                /** @var Gift $gift */
                $gift = Gift::find()->where('quantity > 0')->orderBy('rand()')->one();
                $data = $gift ? $gift->id : 0;
        }

        $toss->data = $data;

        if (!$toss->save()) {
            // TODO: Return failed status with errors
        }

        return $toss;
    }
}