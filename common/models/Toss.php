<?php
/**
 * @author Denysaw
 */
namespace common\models;

/**
 * This is the model class for table "toss".
 *
 * @property int $id
 * @property int $user_id
 * @property int $prize_id
 * @property int $data
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Payout[] $payouts
 * @property Shipment[] $shipments
 * @property Prize $prize
 * @property User $user
 */
class Toss extends ModelBase
{

    const STATUS_CREATED   = 0;
    const STATUS_REFUSED   = 1;
    const STATUS_PENDING   = 2;
    const STATUS_CONVERTED = 3;
    const STATUS_SHIPPED   = 4;
    const STATUS_COMPLETE  = 5;
    const STATUS_FAILED    = 6;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'toss';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'prize_id', 'data'], 'required'],
            [['user_id', 'prize_id', 'data', 'status', 'created_at', 'updated_at'], 'integer'],
            [['prize_id'], 'exist', 'skipOnError' => true, 'targetClass' => Prize::class, 'targetAttribute' => ['prize_id' => 'id']],
            [['user_id'],  'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'prize_id' => 'Prize ID',
            'data' => 'Data',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayouts()
    {
        return $this->hasMany(Payout::class, ['toss_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShipments()
    {
        return $this->hasMany(Shipment::class, ['toss_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrize()
    {
        return $this->hasOne(Prize::class, ['id' => 'prize_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
