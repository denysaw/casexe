<?php
/**
 * @author Denysaw
 */
namespace common\models;

/**
 * This is the model class for table "gift".
 *
 * @property int $id
 * @property string $name
 * @property int $price
 * @property int $quantity
 * @property int $created_at
 * @property int $updated_at
 */
class Gift extends ModelBase
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'gift';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'price'], 'required'],
            [['price', 'quantity', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'price' => 'Price',
            'quantity' => 'Quantity',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
