<?php
/**
 * @author Denysaw
 */
namespace common\jobs;

use common\models\Toss;
use yii\base\BaseObject;
use yii\queue\JobInterface;

class MoneyTransferJob extends BaseObject implements JobInterface
{

    /** @var string */
    public $account;

    /** @var int */
    public $amount;

    /** @var int */
    public $toss_id;


    public function execute($queue)
    {
        $toss    = Toss::findOne(['id' => $this->toss_id]);

        // TODO: implement bank api client and make a tranfer call
        // Yii::app->bankApiClient->transfer($this->account, $this->amount);

        $toss->data -= $this->amount;

        if (!$toss->data) {
            $toss->status = Toss::STATUS_COMPLETE;
        }

        $toss->save();
    }
}