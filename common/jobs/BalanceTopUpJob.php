<?php
/**
 * @author Denysaw
 */
namespace common\jobs;

use common\models\Toss;
use common\models\User;
use yii\base\BaseObject;
use yii\queue\JobInterface;

class BalanceTopUpJob extends BaseObject implements JobInterface
{

    /** @var int */
    public $toss_id;


    public function execute($queue)
    {
        $toss = Toss::findOne(['id' => $this->toss_id]);
        $user = User::findOne(['id' => $toss->user_id]);

        $user->balance += $toss->data;
        $user->save();

        $toss->status = Toss::STATUS_COMPLETE;
        $toss->save();
    }
}