<?php
/**
 * @author Denysaw
 */
use yii\db\Migration;

/**
 * Handles the creation of table `{{%budget}}`.
 */
class m190419_185416_create_budget_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%budget}}', [
            'id'         => $this->primaryKey(),
	        'name'       => $this->string()->notNull()->unique(),
	        'amount'     => $this->integer()->unsigned()->notNull()->defaultValue(0),
	        'created_at' => $this->timestamp(),
	        'updated_at' => $this->timestamp(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%budget}}');
    }
}
