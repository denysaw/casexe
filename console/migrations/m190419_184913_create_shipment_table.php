<?php
/**
 * @author Denysaw
 */
use yii\db\Migration;

/**
 * Handles the creation of table `{{%shipment}}`.
 */
class m190419_184913_create_shipment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%shipment}}', [
            'id' => $this->primaryKey(),
            'user_id'    => $this->integer()->notNull(),
            'toss_id'    => $this->integer(),
            'status'     => $this->smallInteger()->unsigned()->notNull()->defaultValue(0),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
        ]);

        $this->addForeignKey('fk-shipment-user_id', 'shipment', 'user_id', 'user', 'id', 'CASCADE');
        $this->addForeignKey('fk-shipment-toss_id', 'shipment', 'toss_id', 'toss', 'id', 'SET NULL');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%shipment}}');
    }
}
