<?php
/**
 * @author Denysaw
 */
use yii\db\Migration;

/**
 * Handles the creation of table `{{%toss}}`.
 */
class m190419_185532_create_toss_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%toss}}', [
            'id'         => $this->primaryKey(),
	        'user_id'    => $this->integer()->notNull(),
	        'prize_id'   => $this->integer()->notNull(),
	        'data'       => $this->integer()->unsigned()->notNull(),
	        'status'     => $this->smallInteger()->unsigned()->notNull()->defaultValue(0),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
        ]);

	    $this->addForeignKey('fk-toss-user_id',   'toss', 'user_id',   'user',   'id', 'CASCADE');
	    $this->addForeignKey('fk-toss-prize_id',  'toss', 'prize_id',  'prize',  'id', 'RESTRICT');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%toss}}');
    }
}
