<?php
/**
 * @author Denysaw
 */
use yii\helpers\Url;
use common\models\Toss;

/* @var $this yii\web\View */
$this->title = 'ПОБЕДА!';

/** @var Toss $toss */
$prize = $toss->prize;
?>
<div class="site-index">
    <div class="jumbotron">
        <h1>ПОБЕДА!</h1>
        <p class="lead">Вы выиграли приз:</p>
        <h2><?= $prize->getType();?></h2><br>
        <p><?= $prize->getDetails($toss->data);?></p>
        <p>
            <a class="btn btn-sm btn-success" href="<?= Url::toRoute(['end', 'toss_id' => $toss->id, 'status' => Toss::STATUS_PENDING]);?>">Принять приз</a>
            <a class="btn btn-sm btn-danger"  href="<?= Url::toRoute(['end', 'toss_id' => $toss->id, 'status' => Toss::STATUS_REFUSED]);?>">Отказаться от приза</a>
        </p>
    </div>
</div>