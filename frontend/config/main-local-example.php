<?php

$config = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '5xkTp79Q6q8uDBX8UupkNyXBEDfIwq5j',
        ],
        'db' => [
	        'class' => 'yii\db\Connection',
	        'dsn' => 'mysql:host=mysql;dbname=default',
	        'username' => 'default',
	        'password' => 'secret',
	        'charset' => 'utf8',
        ],
    ],
];

if (!YII_ENV_TEST) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
